#zipkin-instrumentation-fetch-hb

[zipkin-instrumentation-fetch](https://github.com/openzipkin/zipkin-js/tree/master/packages/zipkin-instrumentation-fetch) + 
[zipkin-transport-http](https://github.com/openzipkin/zipkin-js/tree/master/packages/zipkin-transport-http) 를 
honeybees 용으로 합쳐놓은 라이브러리

node 기반 Frontend 에서 사용한다. 

[jaeger-client-node](https://github.com/jaegertracing/jaeger-client-node) 와 [opentracing](https://github.com/opentracing/opentracing-javascript) 을 사용하는 것이 가장 이상적이나,
설치 문제로 인해 zipkin 에서 제공하는 라이브러리를 사용함  

소스코드상에 [node-fetch](https://www.npmjs.com/package/node-fetch) 를 대신하여 사용한다. 
`window.fetch` 를 아래 예제와 같이 대체 한다. 

## Install

```
// package.json
...
"zipkin-instrumentation-fetch-hb": "git+https://honeybees2:Peterpass01!@bitbucket.org/honeybeesdev/zipkin-instrumentation-fetch-hb.git"
...
```

## Usage

```javascript
const zipkinFetch = require('zipkin-instrumentation-fetch-hb').getZipkinFetch( JAEGER_ADDRESS, JAEGER_SERVICE_NAME, fetch ); // JAEGER_ADDRESS, JAEGER_SERVICE_NAME 은 webpack.config.js 에서 설정 아래참조

zipkinFetch( 'http://example.com', () => {{
    method: 'GET',
    headers: GlobalDataManager.getHeaders()
}} )
```

### Jaeger Client Setup (각 프로젝트 별 셋업)
Step 1) webpack.config.js, .dev-deploy/webpack.config.dev.js
```
// 통일성을 위해 JAEGER_ADDRESS 는 FLUENT_ADDRESS 뒤에 위치 시킨다.
// JAEGER_SERVICE_NAME 은 const 마지막 줄에 위치시킨다. 
// ...
const FLUENT_ADDRESS = 'https://api-dev.honeybees.co.kr';
const JAEGER_ADDRESS = 'https://jaeger-dev.honeybees.co.kr';
// ...
// ...
const JAEGER_SERVICE_NAME = require( './package.json' ).name; // 당분간 package name 을 사용한다.

module.exports = {
   ...
   plugins: [
       ...
       new webpack.DefinePlugin({
           ...
           FLUENT_ADDRESS: JSON.stringify( FLUENT_ADDRESS ),
           JAEGER_ADDRESS: JSON.stringify( JAEGER_ADDRESS ),
           JAEGER_SERVICE_NAME: JSON.stringify( JAEGER_SERVICE_NAME ),
           ...


```
 
Step 2) .prod-deploy/webpack.config.dev.js (JAEGER_ADDRESS 만 다름)
```
// 통일성을 위해 JAEGER_ADDRESS 는 FLUENT_ADDRESS 뒤에 위치 시킨다.
// JAEGER_SERVICE_NAME 은 const 마지막 줄에 위치시킨다. 
// ...
const FLUENT_ADDRESS = 'https://api.honeybees.co.kr';
const JAEGER_ADDRESS = 'https://jaeger.honeybees.co.kr';
// ...
// ...
const JAEGER_SERVICE_NAME = require( './package.json' ).name; // 당분간 package name 을 사용한다.

module.exports = {
   ...
   plugins: [
       ...
       new webpack.DefinePlugin({
           ...
           FLUENT_ADDRESS: JSON.stringify( FLUENT_ADDRESS ),
           JAEGER_ADDRESS: JSON.stringify( JAEGER_ADDRESS ),
           JAEGER_SERVICE_NAME: JSON.stringify( JAEGER_SERVICE_NAME ),
           ...


```
 
Step 3) src/common/datamanager/* 에 있는 fetch 변경, fetchEx 는 그대로 놔둠 
```
const zipkinFetch = require( 'zipkin-instrumentation-fetch-hb' ).getZipkinFetch( JAEGER_ADDRESS, JAEGER_SERVICE_NAME, fetch );
// ...
// ...
return zipkinFetch( ... 
```
Step 4) FetchEx 안에 있는 fetch 변경
```
const zipkinFetch = require( 'zipkin-instrumentation-fetch-hb' ).getZipkinFetch( JAEGER_ADDRESS, JAEGER_SERVICE_NAME, fetch );
// ...
// ...
return zipkinFetch( ... 
```
