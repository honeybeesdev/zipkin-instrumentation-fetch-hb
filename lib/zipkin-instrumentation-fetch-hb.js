'use strict';
/* eslint-env browser */

/* eslint-disable import/newline-after-import */
// use higher-precision time than milliseconds

process.hrtime = require('browser-process-hrtime');

const {
  Instrumentation,
  Tracer,
  ExplicitContext,
  BatchRecorder,
  jsonEncoder: {
    JSON_V2
  }
} = require('zipkin');

import { HttpLogger } from 'zipkin-transport-http';

function wrapFetch(fetch, _ref) {
  const {
    tracer,
    localServiceName,
    remoteServiceName
  } = _ref;
  const instrumentation = new Instrumentation.HttpClient({
    tracer,
    localServiceName,
    remoteServiceName
  });
  return function zipkinfetch(input) {
    const opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    return new Promise((resolve, reject) => {
      tracer.scoped(() => {
        const url = typeof input === 'string' ? input : input.url;
        const method = opts.method || 'GET';
        const zipkinOpts = instrumentation.recordRequest(opts, url, method);
        const traceId = tracer.id; // console.log( 'url', url );
        // console.log( 'zipkinOpt', zipkinOpts );
        // console.log( 'zipkinOpt.headers', zipkinOpts.headers );
        // console.log( 'X-B3-SpanId', zipkinOpts.headers['X-B3-SpanId']);

        Object.keys(zipkinOpts.headers).forEach(key => {
          zipkinOpts.headers.append(key, zipkinOpts.headers[key]);
        });
        fetch(url, zipkinOpts).then(res => {
          tracer.scoped(() => {
            instrumentation.recordResponse(traceId, res.status);
          });
          resolve(res);
        }).catch(err => {
          tracer.scoped(() => {
            instrumentation.recordError(traceId, err);
          });
          reject(err);
        });
      });
    });
  };
}

function getZipkinFetch(jaegerUrl, localServiceName, nodeFetch) {
  // Send spans to Zipkin asynchronously over HTTP
  const recorder = new BatchRecorder({
    logger: new HttpLogger({
      endpoint: `${jaegerUrl}/api/v2/spans`,
      jsonEncoder: JSON_V2
    })
  });
  const ctxImpl = new ExplicitContext();
  const tracer = new Tracer({
    ctxImpl,
    recorder,
    localServiceName
  });
  return wrapFetch(nodeFetch, {
    tracer,
    localServiceName
  });
}

export default {
  getZipkinFetch
};