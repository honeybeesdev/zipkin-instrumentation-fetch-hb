/**
 * creates jaeger-client tracer
 * fastify version: 1 and 2
 *
 * @param {string} serviceName
 * @returns {Object} tracer
 */
// const { initTracer } = require( 'jaeger-client' );

// const { Tags, FORMAT_HTTP_HEADERS } = require( 'opentracing' );

// function getJaegerTracer( serviceName ) {
//
//     // See schema https://github.com/jaegertracing/jaeger-client-node/blob/master/src/configuration.js#L37
//     const config = {
//         serviceName,
//         reporter: {
//             collectorEndpoint: jaegerEndPoint,
//             logSpans: true
//         },
//         sampler: {
//             type: 'const',
//             param: 1
//         }
//     };
//     const options = {
//         // tags: {
//         //     'tag-name': 'tag-value',
//         // },
//         // metrics: metrics,
//         logger: {
//             info: function logInfo( msg ) {
//                 console.log( 'INFO: ', msg );
//             },
//             error: function logError( msg ) {
//                 console.log( 'ERROR: ', msg );
//             },
//         },
//     };
//
//     return initTracer( config, options );
// }

// const tracer = getJaegerTracer( 'hb-tms-v2' );

// async function jaegerFetch( url, options ) {
//     const span = tracer.startSpan( 'my_function_name' );
//
//     span.setTag( Tags.HTTP_URL, url );
//     span.setTag( Tags.HTTP_METHOD, options.method );
//     span.setTag( Tags.SPAN_KIND, Tags.SPAN_KIND_RPC_CLIENT );
//
//     // Send span context via request headers (parent id etc.)
//     tracer.inject( span, FORMAT_HTTP_HEADERS, options.headers );
//
//     await fetch( url, options );
// }


// module.exports = { wrapFetch: jaegerFetch };
